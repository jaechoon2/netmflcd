﻿using System;
using System.Threading;

using Microsoft.SPOT.Hardware;

using SecretLabs.NETMF.Hardware.Netduino;

namespace Dimps.CharacterLcd
{
    public class Program
    {
        private static Lcd lcd;

        public static void Main()
        {
            InterruptPort button = new InterruptPort(Pins.ONBOARD_SW1, false, Port.ResistorMode.Disabled, Port.InterruptMode.InterruptEdgeBoth);
            button.OnInterrupt += button_OnInterrupt;

            lcd = new Lcd(false) { IsCursorBlinkOn = false, ShowCursor = false, IsDisplayOn = false };
            lcd.WriteString("Quick brown fox jumps over the lazy dog.");
            lcd.IsDisplayOn = true;

            while (true)
            {
                lock (lcd)
                {
                    lcd.ShiftDisplay();
                }
                Thread.Sleep(150);
            }
        }

        private static void button_OnInterrupt(uint data1, uint data2, DateTime time)
        {
            // I am using lock because interrupt can happen any moment, it can happen in the middle of data transfer operation.
            // It would be broken without some sort of thread safety.
            lock (lcd)
            {
                lcd.ResetDisplayAndCursorShift();
            }
        }
    }
}

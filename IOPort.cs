using System;

using Microsoft.SPOT.Hardware;

namespace Dimps.CharacterLcd
{
    public class IOPort : IDisposable
    {
        private readonly TristatePort tristatePort;

        public IOPort(Cpu.Pin portPin, bool initialState, bool glitchFiler, Port.ResistorMode resistor)
        {
            tristatePort = new TristatePort(portPin, initialState, glitchFiler, resistor);
        }

        public void Write(bool state)
        {
            if (!tristatePort.Active)
            {
                tristatePort.Active = true;
            }

            tristatePort.Write(state);
        }

        public bool Read()
        {
            if (tristatePort.Active)
            {
                tristatePort.Active = false;
            }

            return tristatePort.Read();
        }

        public void Dispose()
        {
            tristatePort.Dispose();
        }
    }
}
